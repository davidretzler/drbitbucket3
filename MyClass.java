/**
 * @wi DP-438 in-line DDD EEE FFF GGG HHH IIIBRANCH OOOOO   changexxx
 */
public class MyClass {
  public static void main(String[] args) {
    System.out.println("Hello World");
  }
}
/**
* @wi.XXX DP-348:2428 another line 3
*/
public class MyClass {
  public static void main(String[] args) {
    System.out.println("Goodbye");
  }
}
/**
 * A BB   L in-commit
 */