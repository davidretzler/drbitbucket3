/// @file ADC_System_A429.cpp
///
/// @brief Implements the generic ARINC 429 ADC system class

#include "ADC_System_A429.h"
#include "include/System_Definitions.h"
#include "include/ADC_Errors.h"
#include "ADC_Systems_Module.h"
#include "barocorr.h"
#include <Math_8064-0-Y/math.h>
#include <Util_8041-1-Y/bcd.h>
#include <UASC-STD_8063-0-Y/uasc_stdlib.h>
#include <Util_8041-1-Y/EventLogInterface.h>

#define INHG_HUNDREDTHS_TO_MB_TENTHS(x) ((x) * 10132 / 2992) ///< Inches Hg to millibars (hPa) conversion formula
#define MB_TO_INHG_THOUSANDTHS(x) ((x) * 29914 / 1013)       ///< Millibars (hPa) to inches Hg conversion formula

ADC_System_A429_Class ADC_System_A429[NUMBER_ADC_SYSTEMS];

const uint16_t ADC_System_A429_Class::KNOB_IN_MOTION_TIMEOUT_DIG = (TICKS_PER_SECOND);          ///< Selected target knob-in-motion timeout for an all digital interface, 1 second
const uint16_t ADC_System_A429_Class::KNOB_IN_MOTION_TIMEOUT_ANLG = (TICKS_PER_SECOND * 10);    ///< Selected target knob-in-motion timeout for an analog interface, 10 seconds

/// @brief Constructor for the generic ARINC 429 ADC system class
ADC_System_A429_Class::ADC_System_A429_Class(void)
{
    AirDataCfg.SystemType = ADC_SYSTEM_UNKNOWN;
    /// {ADC_SDD_1230000_0, ADC_SRD_2090435_0}
    ConfigError = Valid_ADCConfigStatus;

    UsingReceivedBaroSetDuringMismatch = true;
}

/// @brief Sets the control limits for each supported bug
///
/// This member function sets which bugs are supported based on the type of
/// ADC configured. It also sets the operational range and resolution for
/// each supported bug. These limits are used to restrict the selected target
/// values commanded by the OFP (or control software) to what the ADC supports.
/// {ADC_SDD_1230000_0, ADC_SRD_2090069_0}
/* 
 * @wi.LinkRoleOne DP-440 cpp rt test
 */
void ADC_System_A429_Class::SetControlLimits(void)
{
    //Target resolutions can be customized to match each ADC's control limitations as necessary.  This value provides a means to
    //check the requested target value resolution against the resolution supported by the external system.  A modulus operation
    //is performed on the target value to check for correct resolution. Unitary resolution indicates the requested target value
    //resolution is expected to match the ADC control resolution for that system.
    ControlLimits.BARO.INHG.Resolution = UNITARY_RESOLUTION; // 0.01 inHg
    ControlLimits.BARO.HPA.Resolution = UNITARY_RESOLUTION; //  1 hPa
    ControlLimits.ASEL.Resolution = 100; // 100 ft increments
    ControlLimits.IAS_BUG.Resolution = UNITARY_RESOLUTION; // 1 knot
    ControlLimits.VS_BUG.Resolution = VS_BUG_RESOLUTION; // 100 ft/min increments
    ControlLimits.MACH_BUG.Resolution = UNITARY_RESOLUTION;

    switch (AirDataCfg.SystemType)
    {
    case ADC_SYSTEM_AC32:
        ControlLimits.BARO.INHG.Minimum = 2067;
        ControlLimits.BARO.INHG.Maximum = 3101;
        ControlLimits.BARO.INHG.IsSupported = true;
        ControlLimits.BARO.HPA.Minimum = 700;
        ControlLimits.BARO.HPA.Maximum = 1050;
        ControlLimits.BARO.HPA.IsSupported = true;
        ControlLimits.ASEL.Minimum = 0;
        ControlLimits.ASEL.Maximum = 55000;
        ControlLimits.ASEL.IsSupported = true;
        ControlLimits.IAS_BUG.Minimum = 0;
        ControlLimits.IAS_BUG.Maximum = 650;
        ControlLimits.IAS_BUG.IsSupported = false;
        ControlLimits.MACH_BUG.Minimum = 0;
        ControlLimits.MACH_BUG.Maximum = 999;
        ControlLimits.MACH_BUG.IsSupported = false;
        ControlLimits.VS_BUG.Minimum = -6000;
        ControlLimits.VS_BUG.Maximum = 6000;
        ControlLimits.VS_BUG.IsSupported = true;
        break;

    case ADC_SYSTEM_AZ800:
        ControlLimits.BARO.INHG.Minimum = 2800;
        ControlLimits.BARO.INHG.Maximum = 3099;
        ControlLimits.BARO.INHG.IsSupported = true;
        ControlLimits.BARO.HPA.Minimum = 948;
        ControlLimits.BARO.HPA.Maximum = 1049;
        ControlLimits.BARO.HPA.IsSupported = true;
        ControlLimits.ASEL.Minimum = 0;
        ControlLimits.ASEL.Maximum = 55000;
        ControlLimits.ASEL.IsSupported = true;
        ControlLimits.IAS_BUG.Minimum = 0;
        ControlLimits.IAS_BUG.Maximum = 650;
        ControlLimits.IAS_BUG.IsSupported = true;
        ControlLimits.MACH_BUG.Minimum = 0;
        ControlLimits.MACH_BUG.Maximum = 999;
        ControlLimits.MACH_BUG.IsSupported = false;
        ControlLimits.VS_BUG.Minimum = -6000;
        ControlLimits.VS_BUG.Maximum = 6000;
        ControlLimits.VS_BUG.IsSupported = true;
        break;

    case ADC_SYSTEM_AZ810:
        ControlLimits.BARO.INHG.Minimum = 2800;
        ControlLimits.BARO.INHG.Maximum = 3099;
        ControlLimits.BARO.INHG.IsSupported = true;
        ControlLimits.BARO.HPA.Minimum = 948;
        ControlLimits.BARO.HPA.Maximum = 1049;
        ControlLimits.BARO.HPA.IsSupported = true;
        ControlLimits.ASEL.Minimum = 0;
        ControlLimits.ASEL.Maximum = 55000;
        ControlLimits.ASEL.IsSupported = true;
        ControlLimits.IAS_BUG.Minimum = 0;
        ControlLimits.IAS_BUG.Maximum = 650;
        ControlLimits.IAS_BUG.IsSupported = true;
        ControlLimits.MACH_BUG.Minimum = 0;
        ControlLimits.MACH_BUG.Maximum = 999;
        ControlLimits.MACH_BUG.IsSupported = false;
        ControlLimits.VS_BUG.Minimum = -6000;
        ControlLimits.VS_BUG.Maximum = 6000;
        ControlLimits.VS_BUG.IsSupported = true;
        break;

    default:
        ControlLimits.BARO.INHG.IsSupported = false;
        ControlLimits.BARO.HPA.IsSupported = false;
        ControlLimits.ASEL.IsSupported = false;
        ControlLimits.IAS_BUG.IsSupported = false;
        ControlLimits.MACH_BUG.IsSupported = false;
        ControlLimits.VS_BUG.IsSupported = false;
        break;
    }

    //set initial targets out of range to trigger bug sync at start up
    ControlInput.ASEL_Target.Value.Data = 0xFFFF;
    ControlInput.IAS_BUG_Target.Value.Data = 999;
    ControlInput.MACH_BUG_Target.Value.Data = 9999;
    ControlInput.VS_BUG_Target.Value.Data = 9999;
    //baro targets will default to zero...

    ControlLimits.IsSet = true;
}

/// @brief Updates the control status of each supported bug
///
/// This member function compares each selected target value against its bug
/// feedback value from the ADC to determine the synchronization state. A bug
/// is considered to be out of synchronization if it's not within a set tolerance
/// after a set amount of time. The tolerances and times are customized to each
/// bug.
/// {ADC_SDD_1230000_0, ADC_SRD_2090074_0, ADC_SRD_2090076_0, ADC_SRD_2090502_20}
// @wi.LinkRoleTwo DP-440 cpp a second instance
void ADC_System_A429_Class::UpdateControlStatus(void)
{
    if (IsConnected())
    {
        //Check for out-of-sync controls, only when a corresponding output is configured
        if (AirDataCfg.BaroPotConnected)
        {
            const uint16_t BARO_POT_INHG_MAX_ERROR = 2; //0.02 inHg

            if (ControlInput.BARO_INHG_Target.Value.Valid)
            {
                if (!RxChannel.GetBaroSetINHG().Valid || (ControlInput.BARO_INHG_Target.KnobInMotionTimer.Expired() && (abs(ControlInput.BARO_INHG_Target.Value.Data - RxChannel.GetBaroSetINHG().Data) > BARO_POT_INHG_MAX_ERROR)))
                {
                    ControlStatus.BARO_OutOfSync = true;
                }
                else
                {
                    ControlStatus.BARO_OutOfSync = false;
                }
            }
            else
            {
                //If the selected target isn't valid, consider it as not supported.
                ControlStatus.BARO_OutOfSync = false;
            }
        }
        else if (TxChannel.IsConnected())
        {
            if (!ControlInput.BaroTargetIsHPA)
            {
                if (ControlInput.BARO_INHG_Target.Value.Valid)
                {
                    if (!RxChannel.GetBaroSetINHG().Valid || (ControlInput.BARO_INHG_Target.KnobInMotionTimer.Expired() && (RxChannel.GetBaroSetINHG().Data != ControlInput.BARO_INHG_Target.Value.Data)))
                    {
                        ControlStatus.BARO_OutOfSync = true;
                    }
                    else
                    {
                        ControlStatus.BARO_OutOfSync = false;
                    }
                }
                else
                {
                    //If the selected target isn't valid, consider it as not supported.
                    ControlStatus.BARO_OutOfSync = false;
                }
            }
            else
            {
                if (ControlInput.BARO_HPA_Target.Value.Valid)
                {
                    if (!RxChannel.GetBaroSetHPA().Valid || (ControlInput.BARO_HPA_Target.KnobInMotionTimer.Expired() && (RxChannel.GetBaroSetHPA().Data != ControlInput.BARO_HPA_Target.Value.Data)))
                    {
                        ControlStatus.BARO_OutOfSync = true;
                    }
                    else
                    {
                        ControlStatus.BARO_OutOfSync = false;
                    }
                }
                else
                {
                    //If the selected target isn't valid, consider it as not supported.
                    ControlStatus.BARO_OutOfSync = false;
                }
            }
        }

        if(TxChannel.IsConnected() || AirDataCfg.ASELSlewConnected)
        {
            if (ControlInput.ASEL_Target.Value.Valid)
            {
                if (!RxChannel.GetSelectedAlt().Valid || (ControlInput.ASEL_Target.KnobInMotionTimer.Expired() && ((RxChannel.GetSelectedAlt().Data >> ALTITUDE_SHIFT) != ControlInput.ASEL_Target.Value.Data)))
                {
                    ControlStatus.ASEL_OutOfSync = true;
                }
                else
                {
                    ControlStatus.ASEL_OutOfSync = false;
                }
            }
            else
            {
                //If the selected target isn't valid, consider it as not supported.
                ControlStatus.ASEL_OutOfSync = false;
            }
        }

        if(AirDataCfg.VerticalSlewConnected)
        {
            if (ControlInput.IAS_BUG_Target.Value.Valid)
            {
                if (!RxChannel.GetAirSpeedBug().Valid || (ControlInput.IAS_BUG_Target.KnobInMotionTimer.Expired() && ((RxChannel.GetAirSpeedBug().Data >> AIRSPEED_SHIFT) != ControlInput.IAS_BUG_Target.Value.Data)))
                {
                    ControlStatus.IAS_BUG_OutOfSync = true;
                }
                else
                {
                    ControlStatus.IAS_BUG_OutOfSync = false;
                }
            }
            else
            {
                //If the selected target isn't valid, consider it as not supported.
                ControlStatus.IAS_BUG_OutOfSync = false;
            }
        }

        if(TxChannel.IsConnected() || AirDataCfg.VerticalSlewConnected)
        {
            if (ControlInput.VS_BUG_Target.Value.Valid)
            {
                if (!RxChannel.GetVertSpeedBug().Valid || (ControlInput.VS_BUG_Target.KnobInMotionTimer.Expired() && (RxChannel.GetVertSpeedBug().Data != ControlInput.VS_BUG_Target.Value.Data)))
                {
                    ControlStatus.VS_BUG_OutOfSync = true;
                }
                else
                {
                    ControlStatus.VS_BUG_OutOfSync = false;
                }
            }
            else
            {
                //If the selected target isn't valid, consider it as not supported.
                ControlStatus.VS_BUG_OutOfSync = false;
            }
        }
    }
}